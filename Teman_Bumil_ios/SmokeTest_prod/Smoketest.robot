*** Setting ***
Library    AppiumLibrary
Library    BuiltIn

Resource    ../Resource/Capability_Device_Resource.robot
Resource    ../Resource/Permission_Resource.robot
Resource    ../Resource/Register_Resource.robot
Resource    ../Resource/Login_Resource.robot
Resource    ../Resource/Home_Resource.robot
Resource    ../Resource/Question_Resource.robot
Resource    ../Resource/DrawerMenu_Resource.robot

# [Teardown]  Close Application

*** Test Cases ***
# 1.Masuk Register Via Login
#   [Tags]    Masuk ke halaman Register dengan klik tombol daftar dihalaman login
#   Buka apps temanbumil real device
#   Masuk Register Via Login
#   Close Application
#
# 3.Register Email Already
#   [Tags]    Register dengan menggunakan email yang sudah terdaftar
#   Register Email Sudah Terdaftar
#   Close Application
#
# 4.Register Email Invalid 1
#   [Tags]    Register dengan menggunakan email yang tidak valid (tanpa .com)
#   Register Email Tidak Valid 1
#   Close Application
#
# 5.Register Email Invalid 2
#   [Tags]    Register dengan menggunakan email yang tidak valid (tanpa @)
#   Register Email Tidak Valid 2
#   Close Application
#
# 6.Register Password Invalid
#   [Tags]    Register dengan menggunakan Password yang tidak valid (kurang dari 6)
#   Register Password Tidak Valid
#   Close Application
#
# 7.Register Not Click Agreement
#   [Tags]    Register dengan tidak memilih setuju pada syarat & ketentuan yang berlaku
#   Register Tidak Pilih Setuju
#   Close Application
#
# 8.Register No Input Mandatory Field
#   [Tags]    Register dengan tidak input mandatory field (tidak input alamat email)
#   Register Tidak Input Mandatory Field
#   Close Application
#
# # 9.Register via Facebook Halaman Login
# #   [Tags]    Register dengan akun facebook dihalaman login
# #
# # 10.Register via Google Halaman Login
# #   [Tags]    Register dengan akun google dihalaman login
# #
# # 11.Register via Facebook Halaman Register
# #   [Tags]    Register dengan akun facebook dihalaman Register
# #
# # 12.Register via Google Halaman Register
# #   [Tags]    Register dengan akun google dihalaman Register

# 13.Login Data Valid
#   [Tags]    Login dengan data login yang valid
#   Login Valid
  # Coachmark Handle Got It
  # Wait Until Page Contains Element    //XCUIElementTypeImage[@name="nav_temanbumil"]    ${timeout}
#   Close Application

# 14.Login Data Email Invalid
#   [Tags]    Login dengan data login email invalid
#   Login Invalid Email
#   Close Application

# 15.Login Data Password Invalid
#   [Tags]    Login dengan data login password invalid
#   Login Invalid Password
#   Close Application

# 16.Lupa Password Email Valid
#   [Tags]    Melakukan lupa password dengan email valid
#   Lupa Password Valid
#   Close Application
#
# 17.Lupa Password Email Invalid
#   [Tags]    Melakukan lupa password dengan email tidak valid
#   Lupa Password Invalid
#   Close Application

18.Program Hamil Homepage
  [Tags]    Login lalu create fertil (sebelumnya hapus dahulu data anak yang ada)
  Login Valid
  # Question Program Hamil
  Coachmark Handle Got It
# 19.Program Hamil Artikel Slider 1
#   [Tags]    membuka artikel slider dihomepage fertil & kembali lagi ke home
#   Homepage Program Hamil Artikel Slider 1
#   Kembali Ke Homepage Fertil Dari Artikel Slider
# 20.Program Hamil Artikel Slider 2
#   [Tags]    membuka artikel slider dihomepage fertil & kembali lagi ke home
#   Homepage Program Hamil Artikel Slider 2
#   Kembali Ke Homepage Fertil Dari Artikel Slider
# 21.Program Hamil checklist
#   [Tags]    checklist & unchecklist list yang ada di homepage, lalu masuk ke dalam see more & checklist lagi. lalu kembali ke homepage fertil
#   Homepage Program Hamil Checklist
#   Kembali Ke Homepage Fertil Dari Checklist
# 22.Program Hamil Tips
#   [Tags]    membuka tips di homepage, scroll sampai bawah lalu kembali ke homepgae fertil
#   Homepage Program Hamil Tips
#   Kembali Ke Homepage Fertil Dari Tips
# 23.Program Hamil Tips Slide
#   [Tags]    slide tips ke tips yang paling ujung di homepage, lalu buka tips & kembali lagi ke homepage fertil
#   Homepage Program Hamil Tips Slide
#   Kembali Ke Homepage Fertil Dari Tips
# 24.Program Hamil Artikel Homepage
#   [Tags]    membuka artikel yang ada di homepage, scroll sampai bawah lalu kembali lagi ke homepage fertil
#   Homepage Program Hamil Artikel
#   Kembali Ke Homepage Fertil Dari Artikel
# 25.Program Hamil Checklist Notifikasi
#   [Tags]    buka notifikasi, pilih lengkapi checklist & masuk ke halaman checklist. lalu kembali lagi ke homepage fertil dengan klik drawer menu home
#   Homepage Program Hamil Checklist Notifikasi
#   Kembali Ke Homepage Fertil Dari Checklist Notifikasi
#   Kembali Ke homapage Fertil via Menu
# 26.Program Hamil Drawer Menu
#   [Tags]    test pada semua fitur yang ada di drawer menu
#   Log    Drawer Menu
# 26a.Program Hamil Drawer Menu Checklist
#   [Tags]    checklist & unchecklist di checklist dari drawer menu checklist
#   Program Hamil Menu Checklis
# 26b.Program Hamil Drawer Menu Agenda
#   [Tags]    create agenda, Open,complete & uncomplete, share & delete
#   Program Hamil Menu Agenda
# 26c.Program Hamil Drawer Menu Album
#   [Tags]    create album, foto, edit album & hapus
#   Program Hamil Menu ALbum
26d.Program Hamil Drawer Menu Artikel - belum selesai
  [Tags]    Open artikel, artikel terkait, search artikel
  Program Hamil Menu Artikel
  Kembali ke List Artikel
  Search Artikel Program Hamil
26e.Program Hamil Drawer Menu Tips
  [Tags]    Open tips, tips terkait, search tips
  Program Hamil Menu Tips
  Kembali ke list tips
  Search Tips Program Hamil
  Kembali ke List Tips Dari Cari
  Kembali Ke Homepage Fertil Dari Drawer Menu
# 26f.Ubah Status Menjadi Sedang Hamil
#   [Tags]    kembali ke Home & pilih sudah hamil
#   Ubah status Menjadi Sudah Hamil
# 26g.Homepage Sedang Hamil Janin 3D
#   [Tags]    Buka Janin 3D & scroll kebawah
#   Sedang Hamil Homapage Janin 3D
