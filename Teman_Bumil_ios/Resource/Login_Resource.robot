*** Setting ***
Library    AppiumLibrary
Library    BuiltIn

Resource    ../Resource/Capability_Device_Resource.robot
Resource    ../Resource/Permission_Resource.robot

*** Variables ***
#Data valid
${EMAIL_LOGIN_VALID}   aniula2@yopmail.com
${PASSWORD_LOGIN_VALID}   123456
#Data Invalid
${EMAIL_LOGIN_INVALID}    aniul2@yopmail.com
${PASSWORD_LOGIN_INVALID}    123456
#Email Forgot Password
${EMAIL_FORGOT_VALID}   aniula@yopmail.com
${EMAIL_FORGOT_INVALID}   aniula200@yopmail.com
#New Password for forgot PASSWORD
${PASSWORD_NEW}   1234567
${PASSWORD_CONFIRM}     1234567

*** Keywords ***
Login Valid
  Buka apps temanbumil real device
  # bila muncul onboarding
  # tampil halaman Login
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@value='Email']    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="LOGIN"]    ${timeout}
  #input alamat email
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@value='Email']    ${timeout}
  Input Value    //XCUIElementTypeTextField[@value='Email']    ${EMAIL_LOGIN_VALID}
  Click Element    //XCUIElementTypeStaticText[@name="Welcome!"]
  # input password
  Wait Until Page Contains Element    //XCUIElementTypeSecureTextField[@value='Password']    ${timeout}
  Input Value    //XCUIElementTypeSecureTextField[@value='Password']    ${PASSWORD_LOGIN_VALID}
  Click Element    //XCUIElementTypeButton[@name="Done"]
  # klik login
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="LOGIN"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="LOGIN"]
  # masuk ke halaman login
  Permission Calendar
  Sleep    1s
  Permission Photo
  Sleep    1s
  Coachmark Handle Got It


Login Invalid Email
  Buka apps temanbumil real device
  # tampil halaman Login
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@value='Email']    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="LOGIN"]    ${timeout}
  #input alamat email
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@value='Email']    ${timeout}
  Input Value    //XCUIElementTypeTextField[@value='Email']    ${EMAIL_LOGIN_INVALID}
  Click Element    //XCUIElementTypeStaticText[@name="Welcome!"]
  # input password
  Wait Until Page Contains Element    //XCUIElementTypeSecureTextField[@value='Password']    ${timeout}
  Input Value    //XCUIElementTypeSecureTextField[@value='Password']    ${PASSWORD_LOGIN_VALID}
  Click Element    //XCUIElementTypeButton[@name="Done"]
  # klik login
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="LOGIN"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="LOGIN"]
  # alert invalid email
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Email tidak ditemukan, periksa kembali penulisan email Anda"]
  Page Should Contain Element    //XCUIElementTypeButton[@name="Ok"]
  Click Element    //XCUIElementTypeButton[@name="Ok"]


Login Invalid Password
  Buka apps temanbumil real device
  # tampil halaman Login
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@value='Email']    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="LOGIN"]    ${timeout}
  #input alamat email
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@value='Email']    ${timeout}
  Input Value    //XCUIElementTypeTextField[@value='Email']    ${EMAIL_LOGIN_VALID}
  Click Element    //XCUIElementTypeStaticText[@name="Welcome!"]
  # input password
  Wait Until Page Contains Element    //XCUIElementTypeSecureTextField[@value='Password']    ${timeout}
  Input Value    //XCUIElementTypeSecureTextField[@value='Password']    ${PASSWORD_LOGIN_INVALID}
  Click Element    //XCUIElementTypeButton[@name="Done"]
  # klik login
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="LOGIN"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="LOGIN"]
  # alert invalid email
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Login gagal. Silakan periksa password kamu!"]
  Page Should Contain Element    //XCUIElementTypeButton[@name="Ok"]
  Click Element    //XCUIElementTypeButton[@name="Ok"]

Lupa Password Valid
  Buka apps temanbumil real device
  # tampil halaman Login
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Lupa sandi?"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="LOGIN"]    ${timeout}
  # masuk ke page lupa Password
  Click Element    //XCUIElementTypeButton[@name="Lupa sandi?"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Forgot password"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@value="Email"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="GANTI KATA SANDI"]   ${timeout}
  # input email
  Input Value    //XCUIElementTypeTextField[@value="Email"]    ${EMAIL_ALREADY}
  Click Element    //XCUIElementTypeStaticText[@name="Forgot password"]
  Click Element    //XCUIElementTypeButton[@name="GANTI KATA SANDI"]
  # ke halaman input kode verifikasi
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Sukses mengganti kata sandi!"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="OK"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="OK"]
  # tampil halaman verifikasi
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Kode Verifikasi Password"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="VERIFIKASI AKUN"]    ${timeout}
  # klik verifikasi akun tanpa input code
  Click Element    //XCUIElementTypeButton[@name="VERIFIKASI AKUN"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="The Code field is required."]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Ok"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Ok"]

Lupa Password Invalid
  Buka apps temanbumil real device
  # tampil halaman Login
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Lupa sandi?"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="LOGIN"]    ${timeout}
  # masuk ke page lupa Password
  Click Element    //XCUIElementTypeButton[@name="Lupa sandi?"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Forgot password"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@value="Email"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="GANTI KATA SANDI"]   ${timeout}
  # input email
  Input Value    //XCUIElementTypeTextField[@value="Email"]    ${EMAIL_LOGIN_INVALID}
  Click Element    //XCUIElementTypeStaticText[@name="Forgot password"]
  Click Element    //XCUIElementTypeButton[@name="GANTI KATA SANDI"]
  # alert
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Permintaan tidak ditemukan, silakan periksa kembali."]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Ok"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Ok"]
