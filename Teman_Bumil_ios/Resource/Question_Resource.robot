*** Setting ***
Library    AppiumLibrary
Library    BuiltIn
Resource    ../Resource/Capability_Device_Resource.robot
Resource    ../Resource/Permission_Resource.robot
Resource    ../Resource/Login_Resource.robot
Resource    ../Resource/Profile_Resource.robot
*** Variables ***
${NAMA_BAYI}    MyBaby
${BERAT_BAYI}   3
${PANJANG_BAYI}   50
${LINGKAR_BAYI}   33
${NAMA_BAYI_EDIT}   MyBaby-Edit
*** Keywords ***
Question Program Hamil
  Cek & Hapus Data Yang Sudah Ada
  Wait Until Page Contains Element    //XCUIElementTypeImage[@name="background pop up"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Silahkan tambah data anak Mums terlebih dahulu"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="OK"]
  # masuk ke question page
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Silahkan pilih Fitur yang Mums inginkan"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Apa yang harus saya pilih?"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Apa yang harus saya pilih?"]
  # halaman apa yang harus saya pilih
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Pilih kategori ini jika Mums berencana untuk hamil lagi atau sedang menjalani program kehamilan."]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="OK, SAYA MENGERTI"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="OK, SAYA MENGERTI"]
  # pilih program hamil
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Silahkan pilih Fitur yang Mums inginkan"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="PROGRAM HAMIL"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="PROGRAM HAMIL"]
  # yakin.?
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Apakah Mums yakin ingin menggunakan fitur Program Hamil?"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Iya"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Iya"]
  # cek create sukses
  Coachmark Handle Got It
  # # Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Program Hamil"]    ${timeout}
  # # Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Profile"]    ${timeout}
  # ke Home klik back - jika sebelumnya buat anak dari profile
  ${profile}    Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Program Hamil"]    ${timeout}
  Run Keyword If    ${profile}    Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton[1]   ${timeout}
  Run Keyword If    ${profile}    Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton[1]
  #cek halaman home
  Wait Until Page Contains Element    //XCUIElementTypeImage[@name="nav_temanbumil"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Jika Mums sudah hamil klik di sini"]   ${timeout}

Cek & Hapus Data Yang Sudah Ada
  ${dataanak}   Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeImage[@name="background pop up"]
  Run Keyword If    ${dataanak}    Coachmark Handle Got It
  Run Keyword Unless    ${dataanak}    Hapus Data Semua Program
