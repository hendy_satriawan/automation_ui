*** Setting ***
Library    AppiumLibrary
Library    BuiltIn
Resource    ../Resource/Capability_Device_Resource.robot
Resource    ../Resource/Permission_Resource.robot
Resource    ../Resource/Login_Resource.robot
Resource    ../Resource/Question_Resource.robot

*** Variables ***
${nama-tambah-anak-sudah-lahir}   Ani sudah lahir

*** Keywords ***
Hapus Data Semua Program
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeButton[1]    200s
  Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeButton[1]
  # tampil menu pilih profile
  Coachmark Handle Got It
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name=" Ani Ina2"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Home"]   ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name=" Ani Ina2"]
  # masuk ke halaman profile & hapus semua data
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Profile"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="options"]    ${timeout}
  : FOR    ${loopCount}    IN RANGE    0    20
  \    Click Element    //XCUIElementTypeButton[@name="options"]
  \    Wait Until Element Is Visible    //XCUIElementTypeButton[@name="Delete"]   20s
  \    Click Element    //XCUIElementTypeButton[@name="Delete"]
  \    Wait Until Element Is Visible    //XCUIElementTypeButton[@name="Ya"]
  \    Click Element    //XCUIElementTypeButton[@name="Ya"]
  \    Wait Until Element Is Visible    //XCUIElementTypeButton[@name="Ok"]
  \    Click Element    //XCUIElementTypeButton[@name="Ok"]
  \    ${data}    Run Keyword And Return Status    Wait Until Page Does Not Contain Element    //XCUIElementTypeButton[@name="options"]   10s
  \    log    ${data}
  \    Run Keyword If    ${data}    Exit For Loop
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    10s
