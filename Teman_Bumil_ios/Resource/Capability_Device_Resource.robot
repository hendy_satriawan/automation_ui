*** Setting ***
Library    AppiumLibrary
Library    BuiltIn
Resource    ../Resource/Permission_Resource.robot

*** Variables ***
# ${REMOTE_URL}     http://localhost:4723/wd/hub
# ${PLATFORM_NAME}    Android
# ${PLATFORM_VERSION_EMULATOR}    6.0
# ${DEVICE_NAME_EMULATOR}    192.168.115.181:5555
# #android 8
# ${PLATFORM_VERSION_REAL_8}      8.0.0
# ${DEVICE_NAME_REAL_8}       520010a1   c051c4cd
# #android 7
# ${PLATFORM_VERSION_REAL_7}      7.1.1
# ${DEVICE_NAME_REAL_7}       192.168.90.222:5555  #49ffe4aa
# # android 6
# ${PLATFORM_VERSION_REAL}      8.0.0    #7.1.1
# ${DEVICE_NAME_REAL}       520010a1c051c4cd    #192.168.90.222:5555  #49ffe4aa
# #
# ${APP}            com.temanbumil.android
# ${APP_PACKAGE}    com.temanbumil.android
# ${APP_ACTIVITY}    com.temanbumil.android.authentication.AuthenticationActivity
# ${APP_ACTIVITY_SPLASH}  com.temanbumil.android.splash.SplashActivity
# ${APP_ACTIVITY_HOME}  com.temanbumil.android.home.HomeActivity
# #
${REMOTE_URL}       http://0.0.0.0:4723/wd/hub
${app}    /Users/hendy.satriawan_gue/Documents/TB_ios_app/TemanBumil_2.app
${bundleId}   com.temanbumil.ios
${version}    12.1
${deviceName}    iphone 7 Plus
${udid}    182D5E3E-F72E-47C3-AE95-B739A596BD8D
${platformName}    iOS
${automationName}    XCUITest
# "app": "/Users/hendy.satriawan_gue/Downloads/D2D.app"

*** Keywords ***
Buka apps temanbumil real device
  Open Application    ${REMOTE_URL}    platformName=${platformName}    platformVersion=${version}    deviceName=${deviceName}    app=${app}   udid=${udid}
  ...    automationName=${automationName}     noReset=False
  Permission Notifications
  Onboarding Handle
