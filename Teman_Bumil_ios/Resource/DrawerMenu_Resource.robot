*** Setting ***
Library    AppiumLibrary
Library    BuiltIn
Library    String

Resource    ../Resource/Capability_Device_Resource.robot
Resource    ../Resource/Permission_Resource.robot
Resource    ../Resource/Home_Resource.robot

*** Variables ***
${judulagenda}    Konsultasi ke Bidan
${isiagenda}   Konsultasi Program Hamil Ke Bidan
${judulagendaedit}    Konsultasi ke Bidan - edit
${namaalbum}    Album Program Hamil
${namaalbumedit}    Album Program Hamil - edit
${judulartikel}   Ini Pertanda Masa Subur
${cariartikel}    Mengenal Infeksi Rahim Penyebab Infertilitas
${judultips}    Alat Deteksi Masa Subur
${caritips}    Cukupi Kebutuhan Zat Besi

*** Keywords ***
Program Hamil Menu Checklis
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeButton[1]   ${timeout}
  Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeButton[1]
  Coachmark Handle Got It
  # buka menu
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Checklist"]   ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Checklist"]
  # check halaman checklist
  Wait Until Page Does Not Contain Element    //XCUIElementTypeActivityIndicator[@name="In progress"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Checklist"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Pemeriksaan Laboratorium"]    ${timeout}
  # checklist
  Program Hamil Checklist

Program Hamil Menu Agenda
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeButton[2]   ${timeout}
  Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeButton[2]
  Coachmark Handle Got It
  # buka menu
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Agenda"]   ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Agenda"]
  #masuk ke agenda
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Agenda"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeImage[@name="add"]   ${timeout}
  # add agenda
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeButton[4]    ${timeout}
  Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeButton[4]
  # masuk ke halaman tambah agenda
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Tambah Agenda"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Simpan"]   ${timeout}
  #input agenda
  # input judul
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@value="Judul"]  ${timeout}
  Input Value    //XCUIElementTypeTextField[@value="Judul"]    ${judulagenda}
  Click Element    //XCUIElementTypeStaticText[@name="Tambah Agenda"]
  # input isi agenda
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@value="Isi Agenda"]   ${timeout}
  Input Value    //XCUIElementTypeTextField[@value="Isi Agenda"]    ${isiagenda}
  Click Element    //XCUIElementTypeButton[@name="Next:"]
  # pilih tanggal
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeOther/XCUIElementTypeTextField[3]    ${timeout}
  Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeOther/XCUIElementTypeTextField[3]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Done"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Done"]
  # pilih waktu kegiatan
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeOther/XCUIElementTypeTextField[4]    ${timeout}
  Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeOther/XCUIElementTypeTextField[4]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Done"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Done"]
  # pilih pengingat
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeOther/XCUIElementTypeTextField[5]    ${timeout}
  Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeOther/XCUIElementTypeTextField[5]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Done"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Done"]
  # klik simpan
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton[2]   ${timeout}
  Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton[2]
  # berhasil simpan
  Wait Until Page Does Not Contain Element    //XCUIElementTypeActivityIndicator[@name="In progress"]   ${timeout}
  # klik oke berhasil
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Teman Bumil"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Proses berhasil!"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Ok"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Ok"]
  # cek agenda yang sudah dibuat
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell  ${timeout}
  Page Should Contain Element    //XCUIElementTypeStaticText[@name="${judulagenda}"]
  # share agenda
  Wait Until Page Contains Element    //XCUIElementTypeImage[@name="share"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeButton[3]    ${timeout}
  Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeButton[3]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Cancel"]
  Wait Until Page Does Not Contain Element    //XCUIElementTypeButton[@name="Cancel"]   ${timeout}
  # buka Agenda
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${judulagenda}"]   ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="${judulagenda}"]
  # masuk ke dalam agenda - ubah jadi complete / uncomplete
  Wait Until Page Contains Element    //XCUIElementTypeImage[@name="share"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${judulagenda}"]   ${timeout}
  # complete
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="COMPLETE"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="COMPLETE"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="UNCOMPLETE"]   ${timeout}
  Page Should Not Contain Element    //XCUIElementTypeStaticText[@name="${judulagenda}"]
  Page Should Contain Element    //XCUIElementTypeStaticText[@name="${isiagenda}"]
  # uncomplete
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="UNCOMPLETE"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="UNCOMPLETE"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="COMPLETE"]   ${timeout}
  # share
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeButton[3]    ${timeout}
  Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeButton[3]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Cancel"]
  # edit agenda
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="options"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="options"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Ubah"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Ubah"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Ubah Agenda"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Simpan"]   ${timeout}
  #hapus judul
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeOther/XCUIElementTypeButton[1]   ${timeout}
  Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeOther/XCUIElementTypeButton[1]
  #edit judul
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@value="Judul Agenda"]   ${timeout}
  Input Value    //XCUIElementTypeTextField[@value="Judul Agenda"]    ${judulagendaedit}
  Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton[2]
  # berhasil edit judul
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Teman Bumil"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Proses berhasil!"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Ok"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Ok"]
  # hapus agenda via home
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeButton[2]    ${timeout}
  Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeButton[2]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Home"]   ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Home"]
  # klik agenda di home
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="AGENDA"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="AGENDA"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${judulagendaedit}"]   ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="${judulagendaedit}"]
  #share agenda
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeButton[3]    ${timeout}
  Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeButton[3]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Cancel"]
  # hapus agenda
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="options"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="options"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Hapus"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Hapus"]
  # sukses hapus
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Proses berhasil!"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Ok"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Ok"]

Program Hamil Menu ALbum
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeButton[1]   ${timeout}
  Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeButton[1]
  #buka menu pilih album
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Album"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Album"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Album"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Simpan foto-foto Mums"]    ${timeout}
  # create album
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Buat Album Baru"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Buat Album Baru"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Buat Album"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Simpan"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@value="Name Album"]   ${timeout}
  Input Value    //XCUIElementTypeTextField[@value="Name Album"]    ${namaalbum}
  Click Element    //XCUIElementTypeButton[@name="Return"]
  Click Element    //XCUIElementTypeButton[@name="Simpan"]
  # berhasil create album
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Teman Bumil"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Proses berhasil!"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Ok"]
  # cek album yang sudah dibuat
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${namaalbum}"]   ${timeout}
  #tambah foto via kamera
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Tambah Foto"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Tambah Foto"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Kamera"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Kamera"]
  Wait Until Page Contains Element    //XCUIElementTypeCell[@name="Moments"]    ${timeout}
  Click Element    //XCUIElementTypeCell[@name="Moments"]
  Wait Until Page Contains Element    //XCUIElementTypeCell[@name="Photo, Landscape, August 09, 2012, 1:52 AM"]   ${timeout}
  Click Element    //XCUIElementTypeCell[@name="Photo, Landscape, August 09, 2012, 1:52 AM"]
  # tambah gambar berhasil
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Proses berhasil!"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Ok"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Ok"]
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[2]/XCUIElementTypeButton[1]    ${timeout}
  #masuk ke detail album
  ${album1}   Get Element Location    //XCUIElementTypeStaticText[@name="${namaalbum}"]
  Log    ${album1}
  ${album1}    Convert To String    ${album1}
  ${remove}   Remove String    ${album1}    {  '   y   x    :   }
  Log    ${remove}
  ${subsx}   Fetch From Right    ${remove}    ,
  ${subsx}   Fetch From Left    ${subsx}    .0
  ${subsy}   Fetch From Left    ${remove}    ,
  ${subsy}  Fetch From Left    ${subsy}    .0
  Log    ${subsx}
  Log    ${subsy}
  Click A Point   x=${subsx}   y=${subsy}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Daftar Album"]   ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Daftar Album"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${namaalbum}"]   ${timeout}
  # tambah gambar
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Tambah Foto"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Tambah Foto"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Kamera"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Kamera"]
  Wait Until Page Contains Element    //XCUIElementTypeCell[@name="Moments"]    ${timeout}
  Click Element    //XCUIElementTypeCell[@name="Moments"]
  Wait Until Page Contains Element    //XCUIElementTypeCell[@name="Photo, Landscape, August 09, 2012, 1:52 AM"]   ${timeout}
  Click Element    //XCUIElementTypeCell[@name="Photo, Landscape, August 09, 2012, 1:52 AM"]
  # tambah gambar berhasil
  Wait Until Page Does Not Contain Element    //XCUIElementTypeActivityIndicator[@name="In progress"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Proses berhasil!"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Ok"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Ok"]
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[3]/XCUIElementTypeOther/XCUIElementTypeImage[1]    ${timeout}
  # edit album
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="options"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="options"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Update"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Update"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Update Album"]   ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Update Album"]
  # edit nama
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton   ${timeout}
  Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@value="Nama Album"]   ${timeout}
  Input Value    //XCUIElementTypeTextField[@value="Nama Album"]    ${namaalbumedit}
  Click Element    //XCUIElementTypeButton[@name="Simpan"]
  # berhasil edit album
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Teman Bumil"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Proses berhasil!"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Ok"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${namaalbumedit}"]   ${timeout}
  # share gambar
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[2]/XCUIElementTypeOther/XCUIElementTypeOther   ${timeout}
  Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[2]/XCUIElementTypeOther/XCUIElementTypeOther
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton[2]    ${timeout}
  Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton[2]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Cancel"]
  # closes gambar
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton[1]    ${timeout}
  Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton[1]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${namaalbumedit}"]   ${timeout}
  # hapus album
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="options"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="options"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Hapus"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Hapus"]
  # verifikasi hapus
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Yakin album di hapus?"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Ya"]
  Click Element    //XCUIElementTypeButton[@name="Ya"]
  # berhasil edit album
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Teman Bumil"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Proses berhasil!"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Ok"]
  # kembali ke halaman album
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Album"]    ${timeout}

Program Hamil Menu Artikel
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton[2]   ${timeout}
  Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton[2]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Artikel"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Artikel"]
  # masuk ke halaman artikel
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Artikel"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="search"]   ${timeout}
  # open artikel
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${judulartikel}"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="${judulartikel}"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${judulartikel}"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="jempol"]   ${timeout}
  # klik like
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeStaticText[@name="You have liked this!"]
  \    Sleep    1s
  \    Run Keyword If    ${el}    Exit For Loop
  \    Click Element    //XCUIElementTypeButton[@name="jempol"]
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Ok"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Ok"]
  # share artikel
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="share grey"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="share grey"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Cancel"]
  # scroll artikel sampai bawah
  ${lebarx}    Get Window Width
  ${tinggiy}   Get Window Height
  ${lebarx}   Convert To Integer    ${lebarx}
  ${tinggiy}  Convert To Integer    ${tinggiy}
  ${lebars}   Evaluate    ${lebarx}/2
  ${tinggis}    Evaluate    ${tinggiy} - 600
  ${x1-artikel}   Convert To String    ${lebars}
  ${x2-artikel}   Convert To String    ${lebars}
  ${y1-artikel}   Convert To String    ${tinggis}
  ${y2-artikel}   Evaluate    ${tinggis} - 700
  #Scroll artikel sampai bawah (sampai dapat artikel terkait)
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeStaticText[@name="Artikel Terkait"]
  \    Sleep    1s
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${x1-artikel}    ${y1-artikel}    ${x2-artikel}    ${y2-artikel}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s

Kembali ke List Artikel
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton[1]    ${timeout}
  Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton[1]
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton[1]    ${timeout}
  Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton[1]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Artikel"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="search"]   ${timeout}

Search Artikel Program Hamil
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="search"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="search"]
  # masuk ke halaman search
  Wait Until Page Contains Element    //XCUIElementTypeImage[@name="icon-search-grey"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="TIPS"]   ${timeout}
  Input Value    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeTextField    ${cariartikel}
  Click Element    //XCUIElementTypeButton[@name="Search"]

Program Hamil Menu Tips
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeButton[2]    ${timeout}
  Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeButton[2]
  #pilih menu tips
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Tips"]   ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Tips"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Tips"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="search"]   ${timeout}
  # open tips
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${judultips}"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="${judultips}"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="share"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${judultips}"]    ${timeout}
  # scroll sampai dapat tips terkait
  ${lebarx}    Get Window Width
  ${tinggiy}   Get Window Height
  ${lebarx}   Convert To Integer    ${lebarx}
  ${tinggiy}  Convert To Integer    ${tinggiy}
  ${lebars}   Evaluate    ${lebarx}/2
  ${tinggis}    Evaluate    ${tinggiy} - 600
  ${x1-tips}   Convert To String    ${lebars}
  ${x2-tips}   Convert To String    ${lebars}
  ${y1-tips}   Convert To String    ${tinggis}
  ${y2-tips}   Evaluate    ${tinggis} - 700
  #Scroll artikel sampai bawah (sampai dapat artikel terkait)
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeStaticText[@name="TIPS TERKAIT"]
  \    Sleep    1s
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${x1-tips}    ${y1-tips}    ${x2-tips}    ${y2-tips}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s
  # share
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="share"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="share"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Cancel"]

Kembali ke list tips
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton[1]   ${timeout}
  Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton[1]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Tips"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="search"]   ${timeout}

Search Tips Program Hamil
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="search"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="search"]
  # masuk halaman cari
  Wait Until Page Contains Element    //XCUIElementTypeImage[@name="icon-search-grey"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="TIPS"]   ${timeout}
  Input Value    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeTextField    ${caritips}
  Click Element    //XCUIElementTypeButton[@name="Search"]
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell/XCUIElementTypeOther[1]   ${timeout}
  Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell/XCUIElementTypeOther[1]
  # masuk tips yang dicari
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${caritips}"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="share"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeImage[@name="dismiss"]   ${timeout}
  # share
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="share"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="share"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Cancel"]

Kembali ke List Tips Dari Cari
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton[1]   ${timeout}
  Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton[1]
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeButton   ${timeout}
  Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeButton
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="search"]   ${timeout}

Kembali Ke Homepage Fertil Dari Drawer Menu
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeButton[2]    ${timeout}
  Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeButton[2]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Home"]   ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Home"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Jika Mums sudah hamil klik di sini"]   ${timeout}
  
