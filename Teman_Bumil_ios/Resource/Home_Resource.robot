*** Setting ***
Library    AppiumLibrary
Library    BuiltIn
Library    String

Resource    ../Resource/Capability_Device_Resource.robot
Resource    ../Resource/Login_Resource.robot
Resource    ../Resource/Question_Resource.robot
Resource    ../Resource/Permission_Resource.robot
*** Variables ***

*** Keywords ***
Kembali Ke Homepage Fertil Dari Artikel Slider

  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeButton[@name="CHECKLIST"]
  \    Run Keyword If    ${el}    Exit For Loop
  \    Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton[1]
  \    Sleep    2s
  \    ${loopCount}    Set Variable    ${loopCount}+1

Homepage Program Hamil Artikel Slider 1
  ${judulslide1}     Get Text    //XCUIElementTypeStaticText[@name="5 Kesalahan Umum saat Menggunakan Test Pack"]
  Log    ${judulslide1}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${judulslide1}"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="5 Kesalahan Umum saat Menggunakan Test Pack"]
  # masuk ke artikel slider
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${judulslide1}"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton[1]   ${timeout}
  # scroll sampai halaman bawah
  ${lebarx}    Get Window Width
  ${tinggiy}   Get Window Height
  ${lebarx}   Convert To Integer    ${lebarx}
  ${tinggiy}  Convert To Integer    ${tinggiy}
  ${lebars}   Evaluate    ${lebarx}/2
  ${tinggis}    Evaluate    ${tinggiy} - 200
  ${x1-artikel}   Convert To String    ${lebars}
  ${x2-artikel}   Convert To String    ${lebars}
  ${y1-artikel}   Convert To String    ${tinggis}
  ${y2-artikel}   Evaluate    ${tinggis} - 700
  #Scroll artikel sampai bawah (sampai dapat artikel dibawah)
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeStaticText[@name="Kesehatan"]
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${x1-artikel}    ${y1-artikel}    ${x2-artikel}    ${y2-artikel}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s

Homepage Program Hamil Artikel Slider 2
  ${judulslide2}     Get Text    //XCUIElementTypeStaticText[@name="Yang Perlu Mums Ketahui Tentang Secondary Infertility"]
  Log    ${judulslide2}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${judulslide2}"]    ${timeout}
  Click Element    ${judulslide2}
  # masuk ke artikel slider
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${judulslide2}"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton[1]   ${timeout}
  # scroll sampai halaman bawah
  ${lebarx}    Get Window Width
  ${tinggiy}   Get Window Height
  ${lebarx}   Convert To Integer    ${lebarx}
  ${tinggiy}  Convert To Integer    ${tinggiy}
  ${lebars}   Evaluate    ${lebarx}/2
  ${tinggis}    Evaluate    ${tinggiy} - 200
  ${x1-artikel}   Convert To String    ${lebars}
  ${x2-artikel}   Convert To String    ${lebars}
  ${y1-artikel}   Convert To String    ${tinggis}
  ${y2-artikel}   Evaluate    ${tinggis} - 700
  #Scroll artikel sampai bawah (sampai dapat artikel dibawah)
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeStaticText[@name="Artikel Terkait"]
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${x1-artikel}    ${y1-artikel}    ${x2-artikel}    ${y2-artikel}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s

Homepage Program Hamil Checklist
  Wait Until Page Contains Element    //XCUIElementTypeImage[@name="nav_temanbumil"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Jika Mums sudah hamil klik di sini"]   ${timeout}
  #scroll cari checklist
  ${lebarx}    Get Window Width
  ${tinggiy}   Get Window Height
  ${lebarx}   Convert To Integer    ${lebarx}
  ${tinggiy}  Convert To Integer    ${tinggiy}
  ${lebars}   Evaluate    ${lebarx}/2
  ${tinggis}    Evaluate    ${tinggiy} - 600
  ${x1-artikel}   Convert To String    ${lebars}
  ${x2-artikel}   Convert To String    ${lebars}
  ${y1-artikel}   Convert To String    ${tinggis}
  ${y2-artikel}   Evaluate    ${tinggis} - 700
  #Scroll artikel sampai bawah (sampai dapat checklist)
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeStaticText[@name="Analisis kromosom untuk mengetahui risiko kelainan kromosom pada janin. "]
  \    Sleep    1s
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${x1-artikel}    ${y1-artikel}    ${x2-artikel}    ${y2-artikel}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s
  # check awal
  # get element checklist untuk di unchecklist
  ${check1}   Get Element Location    //XCUIElementTypeStaticText[@name="Analisis kromosom untuk mengetahui risiko kelainan kromosom pada janin. "]
  Log    ${check1}
  ${check1}    Convert To String    ${check1}
  ${remove}   Remove String    ${check1}    {  '   y   x    :   }
  Log    ${remove}
  ${subsx}   Fetch From Right    ${remove}    ,
  ${subsx}   Fetch From Left    ${subsx}    .0
  ${subsy}   Fetch From Left    ${remove}    ,
  ${subsy}  Fetch From Left    ${subsy}    .0
  Log    ${subsx}
  Log    ${subsy}
  Click Element    //XCUIElementTypeStaticText[@name="Analisis kromosom untuk mengetahui risiko kelainan kromosom pada janin. "]
  Sleep    2s
  Wait Until Page Does Not Contain Element    //XCUIElementTypeActivityIndicator[@name="In progress"]   ${timeout}
  Click A Point   x=${subsx}   y=${subsy}
  Wait Until Page Does Not Contain Element    //XCUIElementTypeActivityIndicator[@name="In progress"]   ${timeout}
  # masuk kedalam checklist
  #scroll cari see more
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeStaticText[@name="SEE MORE"]
  \    Sleep    1s
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${x1-artikel}    ${y1-artikel}    ${x2-artikel}    ${y2-artikel}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Click Element    //XCUIElementTypeStaticText[@name="SEE MORE"]
  Program Hamil Checklist

Program Hamil Checklist
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Checklist"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Pemeriksaan Laboratorium"]   ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Pemeriksaan Laboratorium"]
  # get element checklist untuk di unchecklist
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Analisis kromosom untuk mengetahui risiko kelainan kromosom pada janin. "]
  ${check1}   Get Element Location    //XCUIElementTypeStaticText[@name="Analisis kromosom untuk mengetahui risiko kelainan kromosom pada janin. "]
  Log    ${check1}
  ${check1}    Convert To String    ${check1}
  ${remove}   Remove String    ${check1}    {  '   y   x    :   }
  Log    ${remove}
  ${subsx}   Fetch From Right    ${remove}    ,
  ${subsx}   Fetch From Left    ${subsx}    .0
  ${subsy}   Fetch From Left    ${remove}    ,
  ${subsy}  Fetch From Left    ${subsy}    .0
  Log    ${subsx}
  Log    ${subsy}
  # collapse pemeriksaan laboratorium
  Click Element    //XCUIElementTypeStaticText[@name="Analisis kromosom untuk mengetahui risiko kelainan kromosom pada janin. "]
  Wait Until Page Does Not Contain Element    //XCUIElementTypeActivityIndicator[@name="In progress"]   ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Checklist"]
  Sleep    1s
  Wait Until Page Does Not Contain Element    //XCUIElementTypeActivityIndicator[@name="In progress"]   ${timeout}
  Click A Point   x=${subsx}   y=${subsy}
  # hide pemeriksaan laboratorium
  Wait Until Page Does Not Contain Element    //XCUIElementTypeActivityIndicator[@name="In progress"]   ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Pemeriksaan Laboratorium"]

Kembali Ke Homepage Fertil Dari Checklist
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Checklist"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeButton[1]   ${timeout}
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeButton[@name="CHECKLIST"]
  \    Sleep    1s
  \    Run Keyword If    ${el}    Exit For Loop
  \    Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeButton[1]
  \    ${loopCount}    Set Variable    ${loopCount}+1
  # cek halaman homepage
  Sleep    5s

Homepage Program Hamil Tips
  #scroll cari tips
  ${lebarx}    Get Window Width
  ${tinggiy}   Get Window Height
  ${lebarx}   Convert To Integer    ${lebarx}
  ${tinggiy}  Convert To Integer    ${tinggiy}
  ${lebars}   Evaluate    ${lebarx}/2
  ${tinggis}    Evaluate    ${tinggiy} - 400
  ${x1-artikel}   Convert To String    ${lebars}
  ${x2-artikel}   Convert To String    ${lebars}
  ${y1-artikel}   Convert To String    ${tinggis}
  ${y2-artikel}   Evaluate    ${tinggis} - 700
  #Scroll tips sampai bawah (sampai dapat tips)
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeStaticText[@name="Bolehkah Bercinta saat Haid?"]
  \    Sleep    1s
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${x1-artikel}    ${y1-artikel}    ${x2-artikel}    ${y2-artikel}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s
  # masuk ke dalam tips
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="TIPS"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Bolehkah Bercinta saat Haid?"]   ${timeout}
  ${tips1}  Get Text    //XCUIElementTypeStaticText[@name="Bolehkah Bercinta saat Haid?"]
  Click Element    //XCUIElementTypeStaticText[@name="Bolehkah Bercinta saat Haid?"]
  Wait Until Page Contains    ${tips1}    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="share"]    ${timeout}
  #scroll sampai dapat artikel terkait
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeStaticText[@name="TIPS TERKAIT"]
  \    Sleep    1s
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${x1-artikel}    ${y1-artikel}    ${x2-artikel}    ${y2-artikel}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s
  # masuk ke artikel terkait
  ${tips2}  Get Text    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther[3]/XCUIElementTypeTable/XCUIElementTypeCell[2]
  Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther[3]/XCUIElementTypeTable/XCUIElementTypeCell[2]
  # cek halaman artikel terkait
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="TIPS"]
  #share tips
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="share"]
  Click Element    //XCUIElementTypeButton[@name="share"]
  # cancel share
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]
  Click Element    //XCUIElementTypeButton[@name="Cancel"]

Kembali Ke Homepage Fertil Dari Tips
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeButton[@name="TIPS"]
  \    Sleep    1s
  \    Run Keyword If    ${el}    Exit For Loop
  \    Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton[1]
  \    ${loopCount}    Set Variable    ${loopCount}+1

Homepage Program Hamil Tips Slide
  ${tipsslide}    Get Element Location    //XCUIElementTypeStaticText[@name="Cukupi Kebutuhan Zat Besi"]
  ${tipsslide}    Convert To String    ${tipsslide}
  ${remove}   Remove String    ${tipsslide}    {  '   y   x    :   }
  Log    ${remove}
  ${subsx}   Fetch From Right    ${remove}    ,
  ${subsx}   Fetch From Left    ${subsx}    .0
  ${subsy}   Fetch From Left    ${remove}    ,
  ${subsy}  Fetch From Left    ${subsy}    .0
  ${subsx2}  Fetch From Right    ${remove}    ,
  ${subsx2}   Fetch From Left    ${subsx2}    .0
  Log    ${subsx}
  Log    ${subsy}
  # #ubah value untuk digunakan untuk swipe
  ${subsx2}    Convert To Integer    ${subsx2}
  ${subsx2}   Evaluate    ${subsx2} - 500
  #swipe kesamping untuk dapat tips yang diinginkan
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeStaticText[@name="Kesalahan Umum Penggunaan Testpack"]
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${subsx}    ${subsy}    ${subsx2}    ${subsy}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s
  Click Element    //XCUIElementTypeStaticText[@name="Kesalahan Umum Penggunaan Testpack"]
  #Scroll tips sampai bawah
  ${lebarx}    Get Window Width
  ${tinggiy}   Get Window Height
  ${lebarx}   Convert To Integer    ${lebarx}
  ${tinggiy}  Convert To Integer    ${tinggiy}
  ${lebars}   Evaluate    ${lebarx}/2
  ${tinggis}    Evaluate    ${tinggiy} - 400
  ${x1-artikel}   Convert To String    ${lebars}
  ${x2-artikel}   Convert To String    ${lebars}
  ${y1-artikel}   Convert To String    ${tinggis}
  ${y2-artikel}   Evaluate    ${tinggis} - 700
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeStaticText[@name="TIPS TERKAIT"]
  \    Sleep    1s
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${x1-artikel}    ${y1-artikel}    ${x2-artikel}    ${y2-artikel}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s

Homepage Program Hamil Artikel
  #scroll cari artikel
  ${lebarx}    Get Window Width
  ${tinggiy}   Get Window Height
  ${lebarx}   Convert To Integer    ${lebarx}
  ${tinggiy}  Convert To Integer    ${tinggiy}
  ${lebars}   Evaluate    ${lebarx}/2
  ${tinggis}    Evaluate    ${tinggiy} - 400
  ${x1-artikel}   Convert To String    ${lebars}
  ${x2-artikel}   Convert To String    ${lebars}
  ${y1-artikel}   Convert To String    ${tinggis}
  ${y2-artikel}   Evaluate    ${tinggis} - 700
  #Scroll sampai bawah (sampai dapat artikel)
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[14]
  \    Sleep    1s
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${x1-artikel}    ${y1-artikel}    ${x2-artikel}    ${y2-artikel}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s
  # masuk ke artikel
  Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[14]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="GueSehat"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="share grey"]   ${timeout}
  #scroll sampai dapat artikel terkait
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeStaticText[@name="Artikel Terkait"]
  \    Sleep    1s
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${x1-artikel}    ${y1-artikel}    ${x2-artikel}    ${y2-artikel}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s
  #  buka artikel terkait
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[2]   ${timeout}
  Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[2]
  Wait Until Page Does Not Contain Element    //XCUIElementTypeActivityIndicator[@name="In progress"]   60s
  # cek artikel terkait
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="GueSehat"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="share grey"]   ${timeout}

Kembali Ke Homepage Fertil Dari Artikel
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton[1]   ${timeout}
  #scroll sampai dapat artikel terkait
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeButton[@name="ARTIKEL TERKAIT"]
  \    Sleep    1s
  \    Run Keyword If    ${el}    Exit For Loop
  \    Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton[1]
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s
  Page Should Contain Element    //XCUIElementTypeButton[@name="ARTIKEL TERKAIT"]

Homepage Program Hamil Checklist Notifikasi
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeButton[2]   ${timeout}
  Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeButton[2]
  #masuk halaman notifikasi
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Ayo lengkapi checklist mums."]
  Click Element    //XCUIElementTypeStaticText[@name="Ayo lengkapi checklist mums."]
  # masuk ke halaman checklist
  Program Hamil Checklist

Kembali Ke Homepage Fertil Dari Checklist Notifikasi
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Checklist"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeButton[1]   ${timeout}
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeButton[@name="CHECKLIST"]
  \    Sleep    1s
  \    Run Keyword If    ${el}    Exit For Loop
  \    Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeButton[1]
  \    ${loopCount}    Set Variable    ${loopCount}+1
  # cek halaman homepage
  Sleep    5s

Kembali Ke homapage Fertil via Menu
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeButton[1]   ${timeout}
  Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeButton[1]
  # buka menu
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Home"]   ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Home"]
  # check halaman homepage
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Jika Mums sudah hamil klik di sini"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[2]/XCUIElementTypeButton    ${timeout}

Ubah status Menjadi Sudah Hamil
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Jika Mums sudah hamil klik di sini"]   ${timeout}
  Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[2]/XCUIElementTypeButton
  # verifikasi melanjutkan sudah Hamil
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Selamat atas Kehamilan Mums, silahkan klik Iya untuk melanjutkan"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Iya"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Iya"]
  # masuk question sedang hamil -  pilih belum agar bisa input hpht & masuk maternity dari minggu awal
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Apakah Mums sudah dinyatakan hamil oleh dokter?"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="BELUM"]  ${timeout}
  Click Element    //XCUIElementTypeButton[@name="BELUM"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Apa hasil testpack Mums ?"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="GARIS 1"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="GARIS 1"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Isi HPHT*"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="PILIH"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="PILIH"]
  # pilih tema yang menarik
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Pilih tema yang paling menarik untuk Mums!"]   ${timeout}
  : FOR    ${loopCount}    IN RANGE    11    15
  \    ${el}    Log    ${loopCount}
  \    Sleep    1s
  \    Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[${loopCount}]/XCUIElementTypeButton
  \    Run Keyword If    '${el}' == '14'    Exit For Loop
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    30s
  Click Element    //XCUIElementTypeButton[@name="SUBMIT"]
  # cek halaman maternity
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Selamat atas kehamilan Mums!"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="TUTUP"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="TUTUP"]
  Coachmark Handle Got It

Sedang Hamil Homapage Janin 3D
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeButton[1]   ${timeout}
  Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeButton[1]
  # ${janin}    Get Element Location    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeButton[1]
  # Log    ${janin}
